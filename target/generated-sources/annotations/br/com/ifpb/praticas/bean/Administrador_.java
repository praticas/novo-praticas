package br.com.ifpb.praticas.bean;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Administrador.class)
public abstract class Administrador_ {

	public static volatile SingularAttribute<Administrador, Integer> id;
	public static volatile SingularAttribute<Administrador, String> email;
	public static volatile SingularAttribute<Administrador, String> cpf;
	public static volatile SingularAttribute<Administrador, String> nome;
	public static volatile SingularAttribute<Administrador, String> senha;
	public static volatile SingularAttribute<Administrador, Endereco> endereco;

}

