package br.com.ifpb.praticas.bean;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ImovResidencial.class)
public abstract class ImovResidencial_ {

	public static volatile SingularAttribute<ImovResidencial, Integer> id;
	public static volatile SingularAttribute<ImovResidencial, Integer> qtde_sala;
	public static volatile SingularAttribute<ImovResidencial, String> tipo;
	public static volatile SingularAttribute<ImovResidencial, Integer> qtde_conzinha;
	public static volatile SingularAttribute<ImovResidencial, Double> qtde_metros_quadrados;
	public static volatile SingularAttribute<ImovResidencial, Integer> qtde_quarto;
	public static volatile SingularAttribute<ImovResidencial, Endereco> endereco;

}

