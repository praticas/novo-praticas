package br.com.ifpb.praticas.bean;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ImovComercial.class)
public abstract class ImovComercial_ {

	public static volatile SingularAttribute<ImovComercial, Integer> id;
	public static volatile SingularAttribute<ImovComercial, String> tipo;
	public static volatile SingularAttribute<ImovComercial, Double> qtde_metros_quadrados;
	public static volatile SingularAttribute<ImovComercial, Endereco> endereco;

}

