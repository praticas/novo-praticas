package br.com.ifpb.praticas.bean;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Imobiliaria.class)
public abstract class Imobiliaria_ {

	public static volatile SingularAttribute<Imobiliaria, Integer> id;
	public static volatile SingularAttribute<Imobiliaria, String> email;
	public static volatile SingularAttribute<Imobiliaria, String> nome;
	public static volatile SingularAttribute<Imobiliaria, String> cnpj;
	public static volatile SingularAttribute<Imobiliaria, String> senha;
	public static volatile SingularAttribute<Imobiliaria, Endereco> endereco;

}

