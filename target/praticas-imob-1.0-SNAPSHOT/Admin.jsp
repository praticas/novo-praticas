<!DOCTYPE html>
<html lang="pt-BR" >
    <head>
        <title>Projeto</title>
        <meta charset="utf-8">
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/bootstrap-responsive.css" rel="stylesheet">
        <link href="css/default.css" rel="stylesheet">		
        <link rel="shortcut icon" href="/Simulacao/img/favicon.fw.png" />

        <style type="text/css">
            body{
                padding-top: 40px;
                padding-bottom: 40px;			
            }
            img{float: left;}
            .form-entrar{
                max-width: 300px;
                padding: 19px 29px 29px;
                margin: 0 auto 20px;
                border: 1px solid #e5e5e5;
                border-radius: 5px;
            }
            .form-entrar-titulo{			
                margin-top: 0px;

            }
            .controls{
                padding-bottom: 5px;
                padding-right: 15px;
            }
        </style>
    </head>	
    <body>	
        <div class="container">
            <form action="Controller"  class="form-entrar"  method="post">
                <input type="hidden" name="op" value="LoginAdmin"/> 
                <img alt="�rea do Administrador" title="�rea do Administrador" src="/Simulacao/img/sim.fw.png" oncontextmenu="return false">
                <h2 class="form-entrar-titulo">�rea do Administrador</h2>
                <div class="controls">
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-user"></i></span>
                        <input type="email"  name="email" class="input-block-level" placeholder="E-mail"  />				
                    </div>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-lock"></i></span>				
                        <input type="password" name="senha" class="input-block-level" placeholder="Senha" />
                    </div>
                </div>
                <button class="btn btn-success" type="submit">Entrar</button>
            </form>
        </div><!-- final do Container -->	
        <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="js/bootstrap.min.js"></script>

    </body>

</html>

