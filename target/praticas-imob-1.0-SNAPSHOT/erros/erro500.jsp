<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:import url="header.jsp" />
	<div class="row">
			<div class="span12" style="text-align: center; margin-top: 50px;">
					<div>
						<div class="error-container">
						<h1>500</h1>
						
						<h2>Querido usu�rio, ocorreu um problema interno no nosso servidor.</h2>
						
						<div class="error-details">
							Desculpe o transtorno! 
							Come�e novamente do<a href="/Simulacao/index.jsp"> in�cio</a> ou talvez tente prosseguir!
							
						</div> <!-- /error-details -->
						
						<div class="error-actions" style="margin-top: 10px;">
							<a href="/Simulacao/index.jsp" class="btn btn-large btn-primary">
								<i class="icon-chevron-right"></i>
								
								Prosseguir&nbsp;						
							</a>
							
							
							
						</div> <!-- /error-actions -->
								
					</div> <!-- /error-container -->	
				</div>
			</div>
		</div>			

<c:import url="footer.jsp"></c:import>  