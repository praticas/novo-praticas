<%-- 
    Document   : cadastroAdministrador
    Created on : 03/06/2014, 09:58:52
    Author     : Denismark
--%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:import url="header.jsp" />
<div class="row" id="formulario">
    <div class="span12">
        <center>
            <h1>Cadastro de Administrador</h1>
        </center>
        <form action="Controller" method="post" class="form-horizontal" role="form">
            <input type="hidden" name="op" value="cadastroAdmin"/> 
            <h3>Dados Pessoais</h3>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Nome :</label>
                <div class="col-sm-10">
                    <input type="text" name="nome" class="form-control" id="inputEmail3" placeholder="Nome">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Email :</label>
                <div class="col-sm-10">
                    <input type="email" name="email" class="form-control" id="inputEmail3" placeholder="Email">
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Senha :</label>
                <div class="col-sm-10">
                    <input type="password" name="senha" class="form-control" id="inputPassword3" placeholder="Senha">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Cpf :</label>
                <div class="col-sm-10">
                    <input type="text" name="cpf" class="form-control" id="inputEmail3" placeholder="Cpf">
                </div>
            </div>
            <h3>Endere�o</h3>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Rua :</label>
                <div class="col-sm-10">
                    <input type="text" name="rua" class="form-control" id="inputEmail3" placeholder="Rua">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Bairro :</label>
                <div class="col-sm-10">
                    <input type="text" name="bairro" class="form-control" id="inputEmail3" placeholder="Bairro">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Cep :</label>
                <div class="col-sm-10">
                    <input type="text" name="cep" class="form-control" id="inputEmail3" placeholder="Cep">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Numero :</label>
                <div class="col-sm-10">
                    <input type="text" name="numero" class="form-control" id="inputEmail3" placeholder="Numero">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Cidade :</label>
                <div class="col-sm-10">
                    <input type="text" name="cidade" class="form-control" id="inputEmail3" placeholder="Cidade">
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Estado :</label>
                    <div class="col-sm-10">
                        <input type="text" name="uf" class="form-control" id="inputEmail3" placeholder="Estado">
                    </div>
                </div>
                </br>
                <center>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-primary btn-lg active">Cadastra</button>
                            <a href = "cadastraUsuario.jsp"><button type="button" class="btn btn-default btn-lg active">Cancelar</button></a>
                        </div>
                    </div>
                </center>
        </form>
    </div>
</div>
<c:import url="footer.jsp" />