
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:import url="headerImob.jsp" />

<center>
    <h1>Cadastro de Imovel Residencial</h1>
</center>
<form action="Controller" method="post" class="form-horizontal" role="form">
    <input type="hidden" name="op" value="cadastroImovResidencial"/> 

    <h3>Endere�o</h3>
    <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Rua :</label>
        <div class="col-sm-10">
            <input type="text" name="rua" class="form-control" id="inputEmail3" placeholder="Rua">
        </div>
    </div>

    <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Bairro :</label>
        <div class="col-sm-10">
            <input type="text" name="bairro" class="form-control" id="inputEmail3" placeholder="Bairro">
        </div>
    </div>
    <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Numero :</label>
        <div class="col-sm-10">
            <input type="text" name="numero" class="form-control" id="inputEmail3" placeholder="Numero">
        </div>
    </div>

    <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Cep :</label>
        <div class="col-sm-10">
            <input type="text" name="cep" class="form-control" id="inputEmail3" placeholder="Cep">
        </div>
    </div>
    <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Cidade :</label>
        <div class="col-sm-10">
            <input type="text" name="cidade" class="form-control" id="inputEmail3" placeholder="Cidade">
        </div>
    </div>
    <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Estado :</label>
        <div class="col-sm-10">
            <input type="text" name="uf" class="form-control" id="inputEmail3" placeholder="Estado">
        </div>
    </div>

    <h3>Dados do Imovel</h3>
    <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Quarto(s):</label>
        <div class="col-sm-10">
            <input type="text" name="qtdeQuarto" class="form-control" id="inputEmail3" placeholder="Quantidade Quartos">
        </div>
    </div>
    <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Banheiro(s):</label>
        <div class="col-sm-10">
            <input type="text" name="qtdeBanheiro" class="form-control" id="inputEmail3" placeholder="Quantidade Banheiros">
        </div>
    </div>
    <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Sala(s):</label>
        <div class="col-sm-10">
            <input type="text" name="qtdeSala" class="form-control" id="inputEmail3" placeholder="Quantidade Salas">
        </div>
    </div>


    <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label"> Garagem(s):</label>
        <div class="col-sm-10">
            <input type="text" name="qtdeGaragem"class="form-control" id="inputEmail3" placeholder="Quantidade Garagens">
        </div>
    </div>
    <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Area Coberta :</label>
        <div class="col-sm-10">
            <input type="text" name="area" class="form-control" id="inputEmail3" placeholder="Area em metros Quadrado">
        </div>
    </div>

    </br>
    <center>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button color="black" type="submit" class="btn btn-primary btn-lg active">Cadastra</button>
                <a href="cadastroImovelResidencial.jsp"><button type="button" class="btn btn-default btn-lg active">Cancelar</button></a>
            </div>
        </div>
    </center>
</form>
<c:import url="footer.jsp" />
