<%-- 
    Document   : index
    Created on : 02/06/2014, 11:00:32
    Author     : Denismark
--%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
      <c:import url="headerUsuario.jsp" />
		<div class="row" id="banner">
                    <center>
                        <h3>Login Usuario</h3>                       
                    </center>                 
				<div class="span12">
					<div class="hero-unit">
						<h1>Imobily</h1>
						<p>Imobily e uma site, de ajuda com cadastro e alugueis de imoveis! </p>
						<a href="sobre.jsp" class="btn btn-large" id="btn-hero"> Saiba Mais</a>
					</div>
				</div>
		</div>
<c:import url="footer.jsp" />	
    </body>
</html>