<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:import url="header.jsp" />
	<div class="row">
			<div class="span12" style="text-align: center; margin-top: 50px;">
					<div>
						<div class="error-container">
						<h1>404</h1>
						
						<h2>Querido usu�rio, voc� fez uma solicita��o errada.</h2>
						
						<div class="error-details">
							Desculpe o transtorno, a p�gina requisitada n�o foi encontrada ou n�o existe! 
							Come�e novamente do<a href="/Simulacao/index.jsp"> in�cio</a> ou talvez tente prosseguir!
							
						</div> <!-- /error-details -->
						
						<div class="error-actions" style="margin-top: 10px;">
							<a href="/Simulacao/index.jsp" class="btn btn-large btn-primary">
								<i class="icon-chevron-right"></i>
								
								Prosseguir&nbsp;						
							</a>
							
							
							
						</div> <!-- /error-actions -->
								
					</div> <!-- /error-container -->	
				</div>
			</div>
		</div>			

<c:import url="footer.jsp"></c:import>  