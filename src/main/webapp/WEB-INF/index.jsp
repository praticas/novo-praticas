<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>    

<c:import url="header.jsp" />
		<div class="row" id="banner">
				<div class="span12">
					<div class="hero-unit">
						<h1>Imobily</h1>
						<p>Imobily e uma site, de ajuda com cadastro e alugueis de imoveis! </p>
						<a href="sobre.jsp" class="btn btn-large" id="btn-hero"> Saiba Mais</a>
					</div>
				</div>
		</div>
<c:import url="footer.jsp" />			