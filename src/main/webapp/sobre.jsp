<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>    

<c:import url="header.jsp" />

	<div class="container">
	<div class="row" id="banner">
				<div class="span12">
					<div class="hero-unit">
						<ol>
							
							<li>
									<h4>O que é o Projeto?</h4>
									<p>O projeto foi proposto como método de avaliação da disciplina de PSD
									(Projeto de Sites Dinâmicos) do Curso Superior em Análise  e Desenvolvimento
									de Sistemas do Instituto Federal de Educação, Ciência e Tecnologia
									da Paraíba, campus Cajazeiras. O Sistema SimulaInfo <i>(Simulação, nome
									proposto pela equipe)</i> tem por objetivo o gerenciamento e geração de
									questionários online.
									</p>	
									
							</li>
							
							<li>
									<h4>Mais o que é um questionário online?</h4>
									<p>É um questionário em que você responderá no seu computador. O usuário
									se cadastra e poderá responder várias que questões que serão geradas pelo
									sistema, essas questões são adquiridas através de um banco de questões
									com milhares de questões previamente cadastradas.
									</p>	
									
							</li>
							
							<li>
								
									<h4>Mas quem pode cadastrar questões?</h4>
									<p>Nessa primeira versão (1.0) do sistema só quem tem a permissão de
									gerenciar questões são alguns administradores.
									</p>	
									 
							</li>
							
							<li>
									<h4>Pra quem é voltado os questionários quem podem ser gerados pelo Sistema,
									que tipo de questões existem?</h4>
									<p>O Sistema é respósavel por ajudar a pessoas interessadas em estudar
									materias voltados ao mundo TI, para quem quer se prepar para concursos que
									exige conhecimentos nessa área.
									</p>
							</li>
							
							<li>
									<h4>Posso me cadastrar, quanto custa?</h4>
									<p>Não se preocupe é grátis. Estude, simule, passe!
									</p>
							</li>			
									
									
						</ol>
						
						<div class="row">
							<div class="span12"><a href="cadastraUsuario.jsp" class="btn btn-large"> Cadastre-se</a></div>
						</div>
						
						
					</div>
					
				</div>
			</div>			
	
	</div>


<c:import url="footer.jsp" />