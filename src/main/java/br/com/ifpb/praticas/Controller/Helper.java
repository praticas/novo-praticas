/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ifpb.praticas.Controller;

import br.com.ifpb.praticas.command.CadastroAdminCommand;
import br.com.ifpb.praticas.command.CadastroImobCommand;
import br.com.ifpb.praticas.command.CadastroImovComercialCommand;
import br.com.ifpb.praticas.command.CadastroImovResidencialCommand;
import br.com.ifpb.praticas.command.CadastroUsuarioCommand;
import br.com.ifpb.praticas.command.ICommand;
import br.com.ifpb.praticas.command.LogClientelCommand;
import br.com.ifpb.praticas.command.LoginAdminCommand;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Denismark
 */
public class Helper {

    private static Map<String, ICommand> mapa;
    private static Helper instancia;
  

    private Helper() {
        mapa= new HashMap<String, ICommand>();
        mapa.put("cadastroUsuario", new CadastroUsuarioCommand());
        mapa.put("cadastroImobiliaria", new CadastroImobCommand());
        mapa.put("cadastroAdmin", new CadastroAdminCommand());
        mapa.put("cadastroImovComercial", new CadastroImovComercialCommand());
        mapa.put("cadastroImovResidencial", new CadastroImovResidencialCommand());
        mapa.put("LoginAdmin", new LoginAdminCommand());
        mapa.put("logGeral", new LogClientelCommand());
    }

    public static Helper newInstancia() {
        if (instancia == null) {
            instancia = new Helper();
        }
        return instancia;
    }
    public ICommand getCommand(String key){
        return mapa.get(key);
    }
}
