package br.com.ifpb.praticas.Controller;

import br.com.ifpb.praticas.bean.Cliente;
import br.com.ifpb.praticas.bean.Endereco;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.ifpb.praticas.conexao.ConnectionFactory;
import br.com.ifpb.praticas.dao.DAOGenericoImpl;
import br.com.ifpb.praticas.dao.IDAO;

/**
 * Servlet implementation class Test
 */
public class Test extends HttpServlet {

    private static final long serialVersionUID = 1L;

    public Test() {
        super();

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Cliente cliente = new Cliente();
        Endereco end = new Endereco();
        end.setRua("aaa");
        end.setBairro("ss");
        end.setCidade("gg");
        end.setId(5);
        end.setNumero(2);
        end.setUf("jj");
        cliente.setEndereco(end);
        cliente.setId(5);
        cliente.setNome("Joao");
        cliente.setCpf("043");
        cliente.setEmail("Joao@hotmail.com");
        cliente.setSenha("123");
        IDAO daoJPA = new DAOGenericoImpl();

        daoJPA.save(end);
        daoJPA.save(cliente);

        Cliente c = (Cliente) daoJPA.find(Cliente.class, 1);
        System.out.println(c);
    }

}
