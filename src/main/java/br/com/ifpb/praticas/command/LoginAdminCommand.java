/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ifpb.praticas.command;

import br.com.ifpb.praticas.bean.Administrador;
import br.com.ifpb.praticas.dao.DAOGenericoImpl;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Denismark
 */
public class LoginAdminCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request) {
        Administrador admin = null;
        DAOGenericoImpl dao = new DAOGenericoImpl();
        String email = request.getParameter("email");
        admin = dao.getLoginAdmin(email);

        if ((!email.isEmpty()) || (!email.contains("") || (admin != null))) {
            return "adminLogado.jsp";
        } else {
            return "sobre.jsp";
        }
    }
}

