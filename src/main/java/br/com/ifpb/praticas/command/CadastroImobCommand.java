/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ifpb.praticas.command;

import br.com.ifpb.praticas.bean.Endereco;
import br.com.ifpb.praticas.bean.Imobiliaria;
import br.com.ifpb.praticas.dao.DAOGenericoImpl;
import br.com.ifpb.praticas.dao.IDAO;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Denismark
 */
public class CadastroImobCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request) {
        Imobiliaria imobiliaria = new Imobiliaria();
        Endereco endereco = new Endereco();

        String nome = request.getParameter("nome");
        String email = request.getParameter("email");
        String senha = request.getParameter("senha");
        String cnpj = request.getParameter("cnpj");

        String rua = request.getParameter("rua");
        String bairro = request.getParameter("bairro");
        String numero1 = (request.getParameter("numero"));
        Integer numero = new Integer(numero1);
        String cep = request.getParameter("cep");
        String cidade = request.getParameter("cidade");
        String uf = request.getParameter("uf");

        endereco.setRua(rua);
        endereco.setBairro(bairro);
        endereco.setNumero(numero);
        endereco.setCep(cep);
        endereco.setCidade(cidade);
        endereco.setUf(uf);

        imobiliaria.setNome(nome);
        imobiliaria.setCnpj(cnpj);
        imobiliaria.setEmail(email);
        imobiliaria.setSenha(senha);

        imobiliaria.setEndereco(endereco);

        IDAO dao = new DAOGenericoImpl();
        dao.save(endereco);
        dao.save(imobiliaria);

        return "sobre.jsp";
    }
}
