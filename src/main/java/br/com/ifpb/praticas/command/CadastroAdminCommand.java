/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ifpb.praticas.command;

import br.com.ifpb.praticas.bean.Administrador;
import br.com.ifpb.praticas.bean.Endereco;
import br.com.ifpb.praticas.dao.DAOGenericoImpl;
import br.com.ifpb.praticas.dao.IDAO;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Denismark
 */
public class CadastroAdminCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request) {
        Administrador administrador = new Administrador();
        Endereco endereco = new Endereco();

        String nome = request.getParameter("nome");
        String email = request.getParameter("email");
        String senha = request.getParameter("senha");
        String cpf = request.getParameter("cpf");

        String rua = request.getParameter("rua");
        String bairro = request.getParameter("bairro");
        String nom = (request.getParameter("numero"));
        String cep = request.getParameter("cep");
        Integer numero = new Integer(nom);
        String cidade = request.getParameter("cidade");
        String uf = request.getParameter("uf");

        endereco.setRua(rua);
        endereco.setBairro(bairro);
        endereco.setNumero(numero);
        endereco.setCep(cep);
        endereco.setCidade(cidade);
        endereco.setUf(uf);

        administrador.setNome(nome);
        administrador.setCpf(cpf);
        administrador.setEmail(email);
        administrador.setSenha(senha);

        administrador.setEndereco(endereco);

        IDAO dao = new DAOGenericoImpl();
        dao.save(endereco);
        dao.save(administrador);

        return "sobre.jsp";

    }

}
