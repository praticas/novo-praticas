/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ifpb.praticas.command;

import br.com.ifpb.praticas.bean.Cliente;
import br.com.ifpb.praticas.bean.Imobiliaria;
import br.com.ifpb.praticas.dao.DAOGenericoImpl;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Denismark
 */
public class LogClientelCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request) {
        String login = request.getParameter("login");
        String senha = request.getParameter("senha");

        Cliente cliente = new Cliente();
        DAOGenericoImpl dao = new DAOGenericoImpl();

        if ((login.isEmpty()) || (login.contains(""))) {
            return "sobre.jsp";
        } else {
            cliente = dao.getLoginUsuario(login);
        }
        if (cliente != null) {
            return "usuarioLogado.jsp";
        } else {
            return "sobre.jsp";
        }
    }
}
