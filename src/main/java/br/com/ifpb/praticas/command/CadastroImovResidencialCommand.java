/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ifpb.praticas.command;

import br.com.ifpb.praticas.bean.Cliente;
import br.com.ifpb.praticas.bean.Endereco;
import br.com.ifpb.praticas.bean.ImovResidencial;
import br.com.ifpb.praticas.dao.DAOGenericoImpl;
import br.com.ifpb.praticas.dao.IDAO;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Denismark
 */
public class CadastroImovResidencialCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request) {
        ImovResidencial imovResidencial = new ImovResidencial();
        Endereco endereco = new Endereco();

        String qtdeQuarto1 = request.getParameter("qtdeQuarto");
        Integer qtdeQuarto = new Integer(qtdeQuarto1);
        String qtdeBanheiro1 = request.getParameter("qtdeBanheiro");
        Integer qtdeBanheiro = new Integer(qtdeBanheiro1);
        String qtdeSala1 = request.getParameter("qtdeSala");
        Integer qtdeSala = new Integer(qtdeSala1);
        String qtdeGaragem1 = request.getParameter("qtdeGaragem");
        Integer qtdeGaragem = new Integer(qtdeGaragem1);
        String area1 = request.getParameter("area");
        Double area = new Double(area1);

        String rua = request.getParameter("rua");
        String bairro = request.getParameter("bairro");
        String numero1 = (request.getParameter("numero"));
        Integer numero = new Integer(numero1);
        String cep = request.getParameter("cep");
        String cidade = request.getParameter("cidade");
        String uf = request.getParameter("uf");

        endereco.setRua(rua);
        endereco.setBairro(bairro);
        endereco.setNumero(numero);
        endereco.setCep(cep);
        endereco.setCidade(cidade);
        endereco.setUf(uf);

        imovResidencial.setQtdeBanheiro(qtdeBanheiro);
        imovResidencial.setQtdeGaragem(qtdeGaragem);
        imovResidencial.setQtdeMetrosQuadrados(area);
        imovResidencial.setQtdeQuarto(qtdeQuarto);
        imovResidencial.setQtdeSala(qtdeSala);
        
        imovResidencial.setEndereco(endereco);

        IDAO dao = new DAOGenericoImpl();
        dao.save(endereco);
        dao.save(imovResidencial);

        return "sobre.jsp";

    }

}
