/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.ifpb.praticas.command;

import br.com.ifpb.praticas.bean.Cliente;
import br.com.ifpb.praticas.bean.Endereco;
import br.com.ifpb.praticas.dao.DAOGenericoImpl;
import br.com.ifpb.praticas.dao.IDAO;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Denismark
 */
public class CadastroUsuarioCommand implements ICommand {
    
    @Override
    public String execute(HttpServletRequest request) {
        Cliente cliente = new Cliente();
        Endereco endereco = new Endereco();
        
        String nome = request.getParameter("nome");
        String email = request.getParameter("email");        
        String senha = request.getParameter("senha");
        String cpf = request.getParameter("cpf");
        
        String rua = request.getParameter("rua");
        String bairro = request.getParameter("bairro");
        String numero1 = (request.getParameter("numero"));
        Integer numero = new Integer(numero1);
        String cep = request.getParameter("cep");
        String cidade = request.getParameter("cidade");
        String uf = request.getParameter("uf");
        
        endereco.setRua(rua);
        endereco.setBairro(bairro);
        endereco.setNumero(numero);
        endereco.setCep(cep);
        endereco.setCidade(cidade);
        endereco.setUf(uf);
        
        cliente.setNome(nome);
        cliente.setCpf(cpf);
        cliente.setEmail(email);
        cliente.setSenha(senha);
          
        cliente.setEndereco(endereco);
        
        IDAO dao = new DAOGenericoImpl();
        dao.save(endereco);
        dao.save(cliente);
        
        return "sobre.jsp";
    }
    
}
