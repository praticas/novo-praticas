/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ifpb.praticas.command;

import br.com.ifpb.praticas.bean.Endereco;
import br.com.ifpb.praticas.bean.ImovComercial;
import br.com.ifpb.praticas.dao.DAOGenericoImpl;
import br.com.ifpb.praticas.dao.IDAO;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Denismark
 */
public class CadastroImovComercialCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request) {

        ImovComercial imovComercial = new ImovComercial();
        Endereco endereco = new Endereco();

        String qtdeBanheiro = request.getParameter("qtde_banheiro");
        Integer qtde_Banheiro = new Integer(qtdeBanheiro);
        String area = request.getParameter("area");
        Double qtde_area = new Double(area);
        String qtdeEscritorio = request.getParameter("qtde_escritorio");
        Integer qtde_Escritorio = new Integer(qtdeEscritorio);

        String rua = request.getParameter("rua");
        String bairro = request.getParameter("bairro");
        String nom = (request.getParameter("numero"));
        String cep = request.getParameter("cep");
        Integer numero = new Integer(nom);
        String cidade = request.getParameter("cidade");
        String uf = request.getParameter("uf");

        endereco.setRua(rua);
        endereco.setBairro(bairro);
        endereco.setNumero(numero);
        endereco.setCep(cep);
        endereco.setCidade(cidade);
        endereco.setUf(uf);

        imovComercial.setQtde_banheiro(qtde_Banheiro);
        imovComercial.setQtde_escritorio(qtde_Escritorio);
        imovComercial.setQtde_metros_quadrados(qtde_area);

        imovComercial.setEndereco(endereco);

        IDAO dao = new DAOGenericoImpl();
        dao.save(endereco);
        dao.save(imovComercial);

        return "sobre.jsp";

    }

}
