/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ifpb.praticas.dao;

import br.com.ifpb.praticas.bean.Administrador;
import br.com.ifpb.praticas.bean.Cliente;
import br.com.ifpb.praticas.bean.Imobiliaria;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Denismark
 */
public class DAOGenericoImpl implements IDAO {

    private EntityManager em;
    private EntityManagerFactory emF;

    public DAOGenericoImpl() {
        emF = Persistence.createEntityManagerFactory("praticas");
        em = emF.createEntityManager();
    }

    @Override
    public void save(Object object) {
        em.getTransaction().begin();
        em.persist(object);
        em.getTransaction().commit();
    }

    @Override
    public Object find(Class classe, Object object) {
        return em.find(classe, object);
    }

    @Override
    public void update(Object object) {
        em.getTransaction().begin();
        em.merge(object);
        em.getTransaction().commit();
    }

    @Override
    public void delete(Object object) {

    }

    public Administrador getLoginAdmin(String email) {
        em.getTransaction().begin();
        Administrador admin = null;
        Query query = em.createQuery("select a from Administrador a where a.email = :email");
        System.out.println(email);
        query.setParameter("email", email);
        admin = (Administrador) query.getSingleResult();
        em.getTransaction().commit();

        return admin;
    }

    public Cliente getLoginUsuario(String email) {
        em.getTransaction().begin();
        Cliente cliente = null;
        Query query = em.createQuery("select c from Cliente c where c.email = :email");
        System.out.println(email);
        query.setParameter("email", email);
        cliente = (Cliente) query.getSingleResult();
        em.getTransaction().commit();

        return cliente;
    }

    public Imobiliaria getLoginImob(String email) {
        em.getTransaction().begin();
        Imobiliaria imob = null;
        Query query = em.createQuery("select i from Imobiliaria i where i.email = :email");
        System.out.println(email);
        query.setParameter("email", email);
        imob = (Imobiliaria) query.getSingleResult();
        em.getTransaction().commit();

        return imob;
    }

}
