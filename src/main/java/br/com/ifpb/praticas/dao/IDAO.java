/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.ifpb.praticas.dao;

/**
 *
 * @author Denismark
 */
public interface IDAO {
    public void save(Object object);
    public Object find(Class classe, Object object);
    public void update(Object object);
    public void delete(Object object);    
}
