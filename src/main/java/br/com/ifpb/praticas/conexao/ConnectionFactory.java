package br.com.ifpb.praticas.conexao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {

    private static ConnectionFactory instancia;
    private final String URL = "jdbc:postgresql://localhost:5432/imob";
    private final String DRIVER = "org.postgresql.Driver";
    private final String USER = "postgres";
    private final String PASSWORD = "12345";

    public ConnectionFactory() {

    }

    public static ConnectionFactory newInstance() {
        if (instancia == null) {
            instancia = new ConnectionFactory();
            System.out.println("iniciou conexao ... ");
        }
        return instancia;
    }

    public Connection getConnection() throws ClassNotFoundException, SQLException {
        Class.forName(DRIVER);
        Connection conexao = DriverManager.getConnection(URL, USER, PASSWORD);
        System.out.println("Conectouu ..");
        return conexao;
    }

}
