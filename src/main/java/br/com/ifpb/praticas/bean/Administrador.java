package br.com.ifpb.praticas.bean;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class Administrador implements Serializable {
        @Id
        @GeneratedValue
	private int id;
	private String nome;
	private String cpf;
	private String email;
	private String senha;
        @OneToOne(cascade = CascadeType.ALL)
        @JoinColumn(name = "ENDERECO_ID")
	private Endereco endereco;

	public Administrador(int id, String nome, String cpf, String email,
			String senha, Endereco endereco) {
		super();
		this.id = id;
		this.nome = nome;
		this.cpf = cpf;
		this.email = email;
		this.senha = senha;
		this.endereco = endereco;
	}

	public Administrador() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	@Override
	public String toString() {
		return "Administrador [id=" + id + ", nome=" + nome + ", cpf=" + cpf
				+ ", email=" + email + ", senha=" + senha + ", endereco="
				+ endereco + "]";
	}

}
