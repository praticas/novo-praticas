package br.com.ifpb.praticas.bean;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;

@Entity
public class Reserva implements Serializable {

    @Id
    @GeneratedValue
    private int id;
    @OneToOne
    @JoinColumn(name = "CLIENTE_ID")
    private Cliente cliente;
    @OneToOne
    @JoinColumn(name = "ADMINISTRADOR_ID")
    private Administrador admin;
    @OneToOne
    @JoinColumn(name = "IMOBILIARIA_ID")
    private Imobiliaria imobiliaria;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date data_reserva;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date data_visita;

    public Reserva(int id, Cliente cliente, Administrador admin, Imobiliaria imobiliaria, Date data_reserva, Date data_visita) {
        this.id = id;
        this.cliente = cliente;
        this.admin = admin;
        this.imobiliaria = imobiliaria;
        this.data_reserva = data_reserva;
        this.data_visita = data_visita;
    }

    public Reserva() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Administrador getAdmin() {
        return admin;
    }

    public void setAdmin(Administrador admin) {
        this.admin = admin;
    }

    public Imobiliaria getImobiliaria() {
        return imobiliaria;
    }

    public void setImobiliaria(Imobiliaria imobiliaria) {
        this.imobiliaria = imobiliaria;
    }

    public Date getData_reserva() {
        return data_reserva;
    }

    public void setData_reserva(Date data_reserva) {
        this.data_reserva = data_reserva;
    }

    public Date getData_visita() {
        return data_visita;
    }

    public void setData_visita(Date data_visita) {
        this.data_visita = data_visita;
    }

    @Override
    public String toString() {
        return "Reserva{" + "id=" + id + ", cliente=" + cliente + ", admin=" + admin + ", imobiliaria=" + imobiliaria + ", data_reserva=" + data_reserva + ", data_visita=" + data_visita + '}';
    }

}
