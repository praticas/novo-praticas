package br.com.ifpb.praticas.bean;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class Imobiliaria implements Serializable {
        @Id
        @GeneratedValue
	private int id;
	private String nome;
	private String cnpj;
        @OneToOne
        @JoinColumn(name = "ENDERECO_ID")
	private Endereco endereco;
	private String email;
	private String senha;

	public Imobiliaria(int id, String nome, String cnpj, Endereco endereco,
			String email, String senha) {
		super();
		this.id = id;
		this.nome = nome;
		this.cnpj = cnpj;
		this.endereco = endereco;
		this.email = email;
		this.senha = senha;
	}

	public Imobiliaria() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	@Override
	public String toString() {
		return "Imobiliaria [id=" + id + ", nome=" + nome + ", cnpj=" + cnpj
				+ ", endereco=" + endereco + ", email=" + email + ", senha="
				+ senha + "]";
	}

}
