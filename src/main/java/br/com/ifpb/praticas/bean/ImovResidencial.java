package br.com.ifpb.praticas.bean;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class ImovResidencial implements Serializable {

    @Id
    @GeneratedValue
    private int id;
    @OneToOne
    @JoinColumn(name = "ENDERECO_ID")
    private Endereco endereco;
    private int qtdeQuarto;
    private int qtdeSala;
    private int qtdeBanheiro;
    private int qtdeGaragem;
    private double qtdeMetrosQuadrados;

    public ImovResidencial(int id, Endereco endereco, int qtdeQuarto, int qtdeSala, int qtdeBanheiro, int qtdeGaragem, double qtdeMetrosQuadrados) {
        this.id = id;
        this.endereco = endereco;
        this.qtdeQuarto = qtdeQuarto;
        this.qtdeSala = qtdeSala;
        this.qtdeBanheiro = qtdeBanheiro;
        this.qtdeGaragem = qtdeGaragem;
        this.qtdeMetrosQuadrados = qtdeMetrosQuadrados;
    }

    public ImovResidencial() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public int getQtdeQuarto() {
        return qtdeQuarto;
    }

    public void setQtdeQuarto(int qtdeQuarto) {
        this.qtdeQuarto = qtdeQuarto;
    }

    public int getQtdeSala() {
        return qtdeSala;
    }

    public void setQtdeSala(int qtdeSala) {
        this.qtdeSala = qtdeSala;
    }

    public int getQtdeBanheiro() {
        return qtdeBanheiro;
    }

    public void setQtdeBanheiro(int qtdeBanheiro) {
        this.qtdeBanheiro = qtdeBanheiro;
    }

    public int getQtdeGaragem() {
        return qtdeGaragem;
    }

    public void setQtdeGaragem(int qtdeGaragem) {
        this.qtdeGaragem = qtdeGaragem;
    }

    public double getQtdeMetrosQuadrados() {
        return qtdeMetrosQuadrados;
    }

    public void setQtdeMetrosQuadrados(double qtdeMetrosQuadrados) {
        this.qtdeMetrosQuadrados = qtdeMetrosQuadrados;
    }

    @Override
    public String toString() {
        return "ImovResidencial{" + "id=" + id + ", endereco=" + endereco + ", qtdeQuarto=" + qtdeQuarto + ", qtdeSala=" + qtdeSala + ", qtdeBanheiro=" + qtdeBanheiro + ", qtdeGaragem=" + qtdeGaragem + ", qtdeMetrosQuadrados=" + qtdeMetrosQuadrados + '}';
    }

}
